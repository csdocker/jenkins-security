//Loading Credentials
def env = System.getenv()
def sout = new StringBuffer(), serr = new StringBuffer()
String bucket = env['cibucket']
String hudson_home = env['JENKINS_HOME']
assert "$bucket" && "$hudson_home"
String awss3sync = "aws s3 cp s3://" + bucket + "/GlobalConfiguration/credentials.xml " + hudson_home + "/"
def proc = awss3sync.execute()
proc.consumeProcessOutput(sout, serr)
proc.waitForProcessOutput()
println 'err:'
println serr
println 'out:'
println sout