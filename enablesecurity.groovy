import jenkins.model.*
import hudson.security.*
import hudson.model.User
import com.michelin.cio.hudson.plugins.rolestrategy.*
import org.acegisecurity.acls.sid.PrincipalSid
import groovy.io.FileType

def instance = Jenkins.getInstance()

def securityRealm = new hudson.security.HudsonPrivateSecurityRealm(false, false, null)
instance.setSecurityRealm(securityRealm)
instance.save()

//Loading users
def env = System.getenv()
String bucket = env['cibucket']
String hudson_home = env['JENKINS_HOME']
assert "$bucket" && "$hudson_home"
String awss3sync = "aws s3 sync s3://" + bucket + "/users " + hudson_home + "/users"
def proc = awss3sync.execute()
proc.waitForProcessOutput()

def strategy = new RoleBasedAuthorizationStrategy();

Set<Permission> permissions = new HashSet<Permission>();
groups = new ArrayList<PermissionGroup>(PermissionGroup.getAll());
groups.remove(PermissionGroup.get(Permission.class));

for(group in groups) {
  for(Permission permission : group) {
     permissions.add(permission);
  }
}

def currentDir = new File(hudson_home + '/users')
def allUsers = []
currentDir.eachFile FileType.DIRECTORIES, {
   allUsers << it.name
}

Role adminRole = new Role("admin", permissions);
strategy.addRole(RoleBasedAuthorizationStrategy.GLOBAL, adminRole);

for (u in allUsers) {
   try {
      strategy.assignRole(RoleBasedAuthorizationStrategy.GLOBAL, adminRole, u)   
   }
   catch(org.acegisecurity.userdetails.UsernameNotFoundException e)
   {
      continue
   }
}

instance.setAuthorizationStrategy(strategy)
instance.save()